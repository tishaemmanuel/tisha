#!/usr/bin/python
import sys
sys.path.append('/usr/lib/qubole/duck')
import boto, utils, config, datetime, time, json
from boto.s3.key import Key
import os.path
import zipfile
import difflib
import csv
import requests
import json

def trim_csv_file( PATH ):
  file = open(PATH)
  x = csv.reader(file, delimiter=',', doublequote=1)
  lines = list(x)
  index = [4,5,9,10,14,15,17,18,19,20,21,22,23,24,25]
  data = [ [line[i] for i in index] for line in lines]
  return data

def get_delta(file1, file2):
  data = trim_csv_file(file1)
  with open('/tmp/old.csv', 'w') as old:
    old.writelines(["%s\n" % item  for item in data])
  data = trim_csv_file(file2)
  with  open('/tmp/new.csv', 'w') as new:
    new.writelines(["%s\n" % item  for item in data])
  with open('/tmp/old.csv') as old:
    s1 = set(old)
  with open('/tmp/new.csv') as new:
    s2 = set(new)
  return list(s2-s1)

config.Config.initialize()

# Constants
BUCKET_NAME = 'staging.aws.billing'
ACCOUNT_NUMBER = '330183209093'

##Connect to S3 and get latest csv
MONTH  = time.strftime("%Y-%m")
FILENAME = ACCOUNT_NUMBER + '-aws-billing-detailed-line-items-with-resources-and-tags-' + MONTH + '.csv'

conn = boto.connect_s3(config.Config.get_value("aws_access_key"), config.Config.get_value("aws_secret_key"))
bucket = conn.get_bucket(BUCKET_NAME)
k = Key(bucket)
k.key = FILENAME + '.zip'
k.get_contents_to_filename('/tmp/billing.csv.zip')
archive = zipfile.ZipFile('/tmp/billing.csv.zip')
file = archive.extract(FILENAME)

# If yesterday's file exists, find delta. 
if os.path.exists(FILENAME + '.parsed'):
  parse = get_delta(FILENAME + '.parsed', FILENAME)
else:
  parse = open(file).readlines()

##For each line, get metric and dimention. Makeit into a JSON and upload to signalfx

headers = {"X-SF-Token":"ec_FFTixHl27hda_Ufbxyw",  "Content-Type": "application/json"}

for item in parse:
  line = item.replace("[", "").replace("]", "").split(',') 
  data = {"gauge": [
               {
                 "metric": "InstancePricing", 
                 "value": line[7], 
                 "dimensions": {
                     "product_name": line[1],
                     "type": line[2],
                     "operation": line[3],
                     "usage_start_date": line[4],
                     "usage_end_date": line[5],
                     "resource_id": line[8],
                     "environment": line[9],
                     "name" : line[10],
                  }
              }
          ]
     }
  d = json.dumps(data)
  r = requests.post("https://ingest.signalfx.com/v2/datapoint?orgid=CLJ13XPAgAE", headers = headers, data = d)
  print line
  print r.text

#Rename today's csv 
os.rename(FILENAME, FILENAME + '.parsed')


'''
  record_id = line[0]
  product_name = line[1]
  usage_type = line[2]
  operation = line[3]
  usage_start_date = line[4]
  usage_end_date = line[5]
  rate = line[6]
  cost = line[7]
  resource_id = line[8]
  tag_environment = line[9]
  tag_name = line[10]
  tag_project = line[11]
  tag_qubole = line[12]
  tag_role = line[13]
  tag_alias = line[14]
  print "Record Id: " + record_id + " Product Name " + product_name + " Usage Type: " + usage_type + " Operation" + operation+ " start date : " + usage_start_date + " End date: " + usage_end_date + " Resource_id " + resource_id + " Rate: " + rate + " Cost: " + cost + " Tag_env: " + tag_environment + " Tag_project: " + tag_project + " Tag_qubole: " + tag_qubole + " Tag_role: " + tag_role + " Tag_alias: " + tag_alias
#os.rename(FILENAME, FILENAME + '.parsed')
'''

